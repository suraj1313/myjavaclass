package com.mycompany.app.project;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.File;


public class XmlToJsonDom {
    public static void main(String[] args) {
        try {
        File file = new File("myXml.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = factory.newDocumentBuilder();
        //get document
        Document document = builder.parse(file);




//        System.out.println("Root element :" + document.getDocumentElement().getNodeName());

        if (document.hasChildNodes()) {
            JSONArray json = printNode(document.getChildNodes());
            System.out.println("JSON :" + json);
        }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static JSONArray printNode(NodeList nodeList) {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                if (node.hasChildNodes() && node.getChildNodes().getLength() > 1) {
                    JSONArray temArr = printNode(node.getChildNodes());
                    if (jsonObject.containsKey(node.getNodeName())) {
                        jsonObject.getJSONArray(node.getNodeName()).add(temArr.getJSONObject(0));
                    } else {
                        jsonObject.put(node.getNodeName(), temArr);
                    }
                } else {
                    jsonObject.put(node.getNodeName(), node.getTextContent());
                }
            }
        }
        jsonArray.add(jsonObject);
        return jsonArray;
    }
}
